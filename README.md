# Shopping cart

**Steps for start:**

------------------------------
Для запуска нужно выполнить следующие действия:

- Импортировать поочередно все 4 модуля (app, client, ejb, web) в проект.

- Выставить параметры подулючения к базе тут (я использовал БД Postgres 10.X):

```/src/main/resources/META-INF/persistence.xml```

**Чтобы применились стили из bootstrap, выполнить:**

- ``` cd /home/andrey/IdeaProjects/EE/shop/web/src/main/webapp/assets/libs/bootstrap/css;```

- ``` wget -r https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css```

**Чтобы прогрузить тестовые данные в таблицы, выполнить:**

- ``` sudo psql -d shop -U postgres -a -f ./src/main/resources/db.seed.data.sql; ```
