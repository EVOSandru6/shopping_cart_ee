package kz.shop.web.handlers;

import kz.shop.client.entities.Order;
import kz.shop.ejb.OrderEjb;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.List;

@ManagedBean(name = "orderBean")
@SessionScoped
public class OrderController {
    @Inject
    OrderEjb orderEjb;

    private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

    public String create(CartController cartBean) {
        logger.info("CreateOrderByBean");
        orderEjb.create(cartBean.getItems());
        return "shop?faces-redirect=true";
    }

    public List<Order> getAll() {
        logger.info("CreateOrderByBean");
        return orderEjb.getAll();
    }
}
