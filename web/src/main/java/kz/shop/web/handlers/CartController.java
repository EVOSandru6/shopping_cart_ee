package kz.shop.web.handlers;

import kz.shop.client.models.CartItem;
import kz.shop.client.entities.Product;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "cartBean")
@SessionScoped
@Getter
@Setter
public class CartController {
    private List<CartItem> items;

    private static final Logger logger = LoggerFactory.getLogger(CartController.class);

    public CartController() {
        items = new ArrayList<>();
    }

    public void addToCart(Product product) {
        logger.info("addToCart");

        int index = exists(product);
        if (index == -1) {
            items.add(new CartItem(product, 1));
        } else {
            int quantity = items.get(index).getQty() + 1;
            items.get(index).setQty(quantity);
        }
    }

    public String delete(Product product) {
        int index = exists(product);
        items.remove(index);
        return "cart?faces-redirect=true";
    }

    public double total() {
        double s = 0;
        for (CartItem item : this.items) {
            s += item.getProduct().getPrice().doubleValue() * item.getQty();
        }
        return s;
    }

    private int exists(Product product) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getProduct().getId().equals(product.getId())) {
                return i;
            }
        }
        return -1;
    }
}



