package kz.shop.web.handlers;

import kz.shop.client.entities.Product;
import kz.shop.ejb.ProductEjb;
import kz.shop.web.domain.ProductCriteria;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@ManagedBean(name = "productBean")
@ViewScoped
@Getter
@Setter
public class ProductController {
    @Inject
    private ProductEjb productEjb;

    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    private ProductCriteria criteria;
    private Product newProduct;
    private Product currentProduct;
    private List<Product> productList;

    @PostConstruct
    private void init() {
        newProduct = new Product();
        currentProduct = new Product();
        productList = new ArrayList<>();
        criteria = new ProductCriteria();
    }

    public void availableList() {
        productList = productEjb.availableList();
    }

    public void search() {
        HashMap<String, String> hmap = new HashMap<>();

        if (criteria.getName() != null && !criteria.getName().isEmpty()) {
            logger.info("criteria.getPriceFrom()" + criteria.getPriceFrom());
            hmap.put("name", criteria.getName());
        }
        if (criteria.getPriceFrom() != null && criteria.getPriceFrom().compareTo(new BigDecimal(0, MathContext.DECIMAL64)) >= 0) {
            logger.info("criteria.getPriceTo()" + criteria.getPriceTo());
            hmap.put("priceFrom", criteria.getPriceFrom().toString());
        }
        if (criteria.getPriceTo() != null && criteria.getPriceTo().compareTo(new BigDecimal(0, MathContext.DECIMAL64)) >= 0) {
            logger.info("criteria.getName()" + criteria.getName());
            hmap.put("priceTo", criteria.getPriceTo().toString());
        }

        logger.info("hmapSize(): " + hmap.size());

        productList = productEjb.getListByCriteria(hmap);
    }

    public void findByById() {
        currentProduct = productEjb.findByPk(currentProduct.getId());
    }

    public String create() {
        logger.info("CreateProduct");

        FacesContext ctx = FacesContext.getCurrentInstance();
        checkAddingValidation(ctx);
        if (ctx.getMessageList().size() > 0) {
            return null;
        }
        logger.info("beforeCreate");
        productEjb.create(newProduct);
        logger.info("afterCreate");

        flashSuccessAndRefreshProduct(ctx);
        return "shop?faces-redirect=true";
    }

    private void flashSuccessAndRefreshProduct(FacesContext ctx) {
        ctx.addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Success",
                        "Product " + newProduct.getName() + " successfully added."));

        newProduct = new Product();
    }

    private void checkAddingValidation(FacesContext ctx) {
        if (newProduct.getName() == null || "".equals(newProduct.getName())) {
            ctx.addMessage("productForm:name",
                    new FacesMessage(FacesMessage.SEVERITY_WARN, "Неверное значение", "Вы должны ввести name"));
        }

        if (newProduct.getPrice() == null || "".equals(newProduct.getPrice())) {
            ctx.addMessage("productForm:price",
                    new FacesMessage(FacesMessage.SEVERITY_WARN, "Неверное значение", "Вы должны ввести price"));
        }

        if (newProduct.getBody() == null || "".equals(newProduct.getBody())) {
            ctx.addMessage("productForm:body",
                    new FacesMessage(FacesMessage.SEVERITY_WARN, "Неверное значение", "Вы должны ввести body"));
        }
    }

    public void clear() {
        criteria = new ProductCriteria();
    }
}
