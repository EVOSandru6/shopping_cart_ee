package kz.shop.web.domain;

import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;

@Getter
@Setter
public class ProductCriteria {
    private String name;
    private BigDecimal priceFrom;
    private BigDecimal priceTo;
}
