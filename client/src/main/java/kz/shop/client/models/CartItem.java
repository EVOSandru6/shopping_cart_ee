package kz.shop.client.models;

import kz.shop.client.entities.Product;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CartItem {
    private Product product;
    private int qty;

    public CartItem() {
    }

    public CartItem(Product product, int qty) {
        this.product = product;
        this.qty = qty;
    }
}
