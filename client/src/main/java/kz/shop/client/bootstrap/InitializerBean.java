package kz.shop.client.bootstrap;

import kz.shop.client.exceptions.DomainException;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.resource.ResourceAccessor;
import org.postgresql.ds.PGPoolingDataSource;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;

//import kz.shop.exceptions.DomainExceptionOlolo;

@Startup
@Singleton
@TransactionManagement(TransactionManagementType.BEAN)
public class InitializerBean {
    private static final String STAGE = "development";
//    private static final String CHANGELOG_FILE = "src/main/resources/liquibase/changelog-master.xml";
     private static final String CHANGELOG_FILE = "liquibase/changelog-master.xml";

    // todo свести с pesistence.xml
    // @Resource
    // private DataSource ds;

    @PostConstruct
    protected void bootstrap() throws DomainException {
        System.out.println("InitializerBean:bootstrap:it");
        // todo Пока через Maven либо хардкодным PGPoolingDataSource
        startMigrations();
    }

    private void startMigrations() throws DomainException {
        ResourceAccessor resourceAccessor = new ClassLoaderResourceAccessor(getClass().getClassLoader());

        System.out.println("stepx0");

        PGPoolingDataSource ds = new org.postgresql.ds.PGPoolingDataSource();
        ds.setUrl("jdbc:postgresql://localhost:5432/shop");
        ds.setUser("postgres");
        ds.setPassword("postgres");
        ds.setInitialConnections(10);
        ds.setMaxConnections(20);
        ds.setSslMode("require");
        ds.setSslfactory("org.postgresql.ssl.NonValidatingFactory");

        System.out.println("stepx1");
        try (Connection connection = ds.getConnection()) {
            JdbcConnection jdbcConnection = new JdbcConnection(connection);

            System.out.println("stepx3");

            System.out.println("jdbcConnection.getConnectionUserName():::" + jdbcConnection.getConnectionUserName());
            System.out.println("jdbcConnection.getConnectionUserName():::" + jdbcConnection.getDatabaseProductName());
            System.out.println("jdbcConnection.getConnectionUserName():::" + jdbcConnection.getDatabaseProductVersion());
            System.out.println("jdbcConnection.getConnectionUserName():::" + jdbcConnection.getDatabaseMajorVersion());

            Database db = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
            Liquibase liquiBase = new Liquibase(CHANGELOG_FILE, resourceAccessor, db);
            liquiBase.update(STAGE);
        } catch (SQLException | LiquibaseException e) {
            throw new DomainException("Invalid DB");
        }
    }
}
