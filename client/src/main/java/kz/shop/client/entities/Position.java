package kz.shop.client.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "positions")
@NamedQueries(
        @NamedQuery(name = Position.Query.FIND_BY_ORDER, query = "SELECT o FROM Position o WHERE orderId=:orderId ORDER BY o.id")
)
@Getter
@Setter
public class Position {
    public static class Query {
        public static final String FIND_BY_ORDER = "Position.findByOrder";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private BigInteger id;

    @Column(name = "product_id")
    private BigInteger productId;

    @Column(name = "order_id")
    private BigInteger orderId;

    @Column
    private Integer qty;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    private Product product;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "order_id", referencedColumnName = "id")
    private Order order;
}
