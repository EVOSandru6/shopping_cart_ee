package kz.shop.client.entities;

import lombok.Getter;
import lombok.Setter;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "orders")
@NamedQueries(
        @NamedQuery(name = Order.Query.FIND_ALL, query = "SELECT o FROM Order o ORDER BY o.id")
)
@Getter
@Setter
public class Order {
    public static class Query {
        public static final String FIND_ALL = "Order.findAll";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private BigInteger id;

    @Column(name = "created_at", columnDefinition = "DATE")
    private LocalDateTime createdAt;

    @Column
    private Integer status;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "positions", joinColumns = @JoinColumn(name = "order_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id"))
    private List<Product> products = new ArrayList<>();
}
