package kz.shop.client.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.PostConstruct;
import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;

@Entity
@Table(name = "products")
@NamedQueries({
        @NamedQuery(name = Product.Query.FIND_COMMON, query = "SELECT p FROM Product p"),
        @NamedQuery(name = Product.Query.FIND_ALL_EXISTS_ON_STOCK, query = "SELECT p FROM Product p WHERE EXISTS(SELECT s FROM Stock s WHERE s.productId = p.id and s.qty > 0)"),
})
@Getter
@Setter
@ToString
public class Product {
    public static class Query {
        public static final String FIND_COMMON = "Product.findCommon";
        public static final String FIND_ALL_EXISTS_ON_STOCK = "Product.findAllExistsOnStock";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private BigInteger id;
    @Column
    private String name;
    @Column
    private String photo;
    @Column
    private BigDecimal price;
    @Column
    private String body;
}
