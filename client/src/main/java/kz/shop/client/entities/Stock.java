package kz.shop.client.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "stock")
@NamedQueries(
        @NamedQuery(name = Stock.Query.FIND_ALL, query = "SELECT s FROM Stock s ORDER BY s.id")
)
@Getter
@Setter
public class Stock {
    public static class Query {
        public static final String FIND_ALL = "Stock.findAll";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private BigInteger id;

    @Column(name="product_id")
    private BigInteger productId;

    @Column
    private Integer qty;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    private Product product;

    @Override
    public String toString() {
        return "Stock{" +
                "id=" + id +
                ", productId='" + productId + '\'' +
                ", qty=" + qty +
                ", newProduct=" + product +
                '}';
    }
}
