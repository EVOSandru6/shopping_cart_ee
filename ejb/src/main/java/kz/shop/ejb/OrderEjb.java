package kz.shop.ejb;

import kz.shop.client.entities.Order;
import kz.shop.client.entities.Position;
import kz.shop.client.models.CartItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDateTime;
import java.util.List;

@Stateless
public class OrderEjb {
    private static final Logger logger = LoggerFactory.getLogger(OrderEjb.class);
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");

    public List<Order> getAll() {
        EntityManager em = emf.createEntityManager();
        return em.createNamedQuery(Order.Query.FIND_ALL, Order.class).getResultList();
    }

    public void create(List<CartItem> items) {
        logger.info("CreateOrder: " + items.size());
        EntityManager em = emf.createEntityManager();
        Order order = new Order();
        order.setCreatedAt(LocalDateTime.now());
        order.setStatus(1);
        em.persist(order);

        logger.info("CreateOrderDone: " + order.getId());

        for (CartItem item : items) {
            logger.info("StartPositionCreate.");
            Position position = new Position();
            position.setOrder(order);
            logger.info("StartPositionCreateStep2.");
            position.setProductId(item.getProduct().getId());
//            position.setProduct(item.getProduct());
            logger.info("StartPositionCreateStep3.");
            position.setQty(item.getQty());
            logger.info("position.getProduct: " + position.getProduct());
            em.persist(position);
            logger.info("FinishPositionCreate.");
        }
        logger.info("OrderItemsDone.");
    }
}
