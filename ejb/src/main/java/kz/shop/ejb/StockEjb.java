package kz.shop.ejb;

import kz.shop.client.entities.Product;
import kz.shop.client.entities.Stock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.math.BigInteger;

@Stateless
public class StockEjb {
    private static final Logger logger = LoggerFactory.getLogger(StockEjb.class);
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");

    public void attachProduct(BigInteger product_id) {
        // public void attachProduct(Product product) {

        logger.info("attachProduct: " + product_id);

        EntityManager em = emf.createEntityManager();
        Stock stock = new Stock();
        stock.setQty(1);
        stock.setProductId(product_id);
        // stock.setProduct(product);
        em.persist(stock);

        logger.info("attachProduct - finished");
    }
}
