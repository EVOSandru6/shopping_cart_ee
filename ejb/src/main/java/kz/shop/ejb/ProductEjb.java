package kz.shop.ejb;

import kz.shop.client.entities.Product;
import kz.shop.client.entities.Stock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Stateless
public class ProductEjb {
    private static final Logger logger = LoggerFactory.getLogger(ProductEjb.class);
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");

    public List<Product> availableList() {
        EntityManager em = emf.createEntityManager();
        return em.createNamedQuery(Product.Query.FIND_ALL_EXISTS_ON_STOCK, Product.class).getResultList();
    }

    public void create(Product product) {
        logger.info("createProduct. Step1: " + product);
        product.setPhoto("https://picsum.photos/50/50?grayscale");
        EntityManager em = emf.createEntityManager();
        em.persist(product);
        logger.info("createProduct. Step2: " + product);

        attachProductToStock(product, em);
    }

    private void attachProductToStock(Product product, EntityManager em) {
        Stock stock = new Stock();
        stock.setQty(1);
        stock.setProduct(product);
        em.persist(stock);
    }

    public Product findByPk(BigInteger id) {
        EntityManager em = emf.createEntityManager();
        return em.find(Product.class, id);
    }

    public List<Product> getListByCriteria(Map<String, String> criteriaMap) {
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Product> query = cb.createQuery(Product.class);
        Root<Product> root = query.from(Product.class);

        List<Predicate> predicates = getPredicatesToCtiteria(criteriaMap, cb, root);

        query.select(root).where(predicates.toArray(new Predicate[]{}));

        logger.info("queryResult: " + query);

        List<Product> products = em.createQuery(query).getResultList();

        return products;
    }

    private List<Predicate> getPredicatesToCtiteria(Map<String, String> criteriaMap, CriteriaBuilder cb, Root<Product> root) {
        List<Predicate> predicates = new ArrayList<>();

        criteriaMap.forEach((key, value) -> {
            logger.info("criteriaMap: " + key + ", " + value);

            switch (key) {
                case "name":
                    predicates.add(
                            cb.like(cb.lower(root.get("name")), "%" + value.toLowerCase() + "%")
                    );
                    break;
                case "priceFrom":
                    predicates.add(
                            cb.ge(root.get("price"), new BigDecimal(value, MathContext.DECIMAL64))
                    );
                    break;
                case "priceTo":
                    logger.info("priceToValue: " + value);
                    if (Double.parseDouble(value) != 0.00) {
                        predicates.add(
                                cb.le(root.get("price"), new BigDecimal(value, MathContext.DECIMAL64))
                        );
                    }
                    break;
            }
        });
        return predicates;
    }
}
